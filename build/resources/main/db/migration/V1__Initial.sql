CREATE TABLE IF NOT EXISTS author
(
    id              BIGSERIAL PRIMARY KEY,
    age             INT,
    name            VARCHAR(64) NOT NULL UNIQUE,
    surname         VARCHAR(64),
    created_at      TIMESTAMP DEFAULT now(),
    updated_at      TIMESTAMP DEFAULT now()
);

CREATE TABLE IF NOT EXISTS book
(
    id      BIGSERIAL PRIMARY KEY,
    name    VARCHAR(64) NOT NULL UNIQUE,
    page    INT,
    author_id BIGINT NOT NULL REFERENCES author(id) ON DELETE SET NULL,
    created_at     TIMESTAMP DEFAULT now(),
    updated_at     TIMESTAMP DEFAULT now()
);
