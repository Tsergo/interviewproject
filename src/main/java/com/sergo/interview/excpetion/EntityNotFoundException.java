package com.sergo.interview.excpetion;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class EntityNotFoundException extends RuntimeException {

	public EntityNotFoundException() {
		super("Entity with given parameters not found");
	}

	public EntityNotFoundException(String message) {
		super(message);
	}

}
