package com.sergo.interview.mapper;

import com.sergo.interview.data.entity.author.Author;
import com.sergo.interview.dto.author.AuthorDTO;
import org.mapstruct.*;

@Mapper(componentModel = "spring", uses = BookMapper.class, unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface AuthorMapper {

	AuthorDTO authorDTOFromAuthor(Author author);

	@Mappings({
			@Mapping(target = "id", ignore = true),
			@Mapping(target = "createdAt", ignore = true),
			@Mapping(target = "updatedAt", ignore = true),
	})
	Author updateAuthor(@MappingTarget Author author, AuthorDTO authorD);

	Author authorFromAuthorDTO(AuthorDTO authorDTO);

}
