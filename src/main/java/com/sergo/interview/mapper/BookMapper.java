package com.sergo.interview.mapper;

import com.sergo.interview.data.entity.book.Book;
import com.sergo.interview.dto.book.BookDTO;
import org.mapstruct.*;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring", uses = AuthorMapper.class, unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface BookMapper {

	@Mappings({
			@Mapping(target = "createdAt", ignore = true),
			@Mapping(target = "updatedAt", ignore = true),
	})
	Book bookFromBookDTO(BookDTO bookDTO);

	@Mappings({
			@Mapping(target = "author", ignore = true)
	})
	@Named("iterateList")
	BookDTO bookDTOFromBook(Book book);

	@IterableMapping(qualifiedByName="iterateList")
	List<BookDTO> bookDTOListFromBookList(List<Book> books);

}
