package com.sergo.interview.data.entity.book;

import com.sergo.interview.data.entity.AbstractEntity;
import com.sergo.interview.data.entity.author.Author;

import javax.persistence.*;

@Entity
@Table(name = "book")
public class Book extends AbstractEntity {

	@Column(name = "name")
	private String name;

	@Column(name = "page")
	private Integer page;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "author_id")
	private Author author;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}
}
