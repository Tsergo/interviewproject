package com.sergo.interview.data.repository;

import com.sergo.interview.data.entity.author.Author;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

public class AuthorRepositoryImpl implements AuthorRepositoryCustom {

	@PersistenceContext
	EntityManager entityManager;

	@Override
	public List<Author> findAllByCustomQuery() {
		Query q = entityManager.createNativeQuery("" ,Author.class);

		return q.getResultList();
	}
}
