package com.sergo.interview.data.repository;

import com.sergo.interview.data.entity.author.Author;

import java.util.List;

public interface AuthorRepositoryCustom {

	List<Author> findAllByCustomQuery();
}
