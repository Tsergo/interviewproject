package com.sergo.interview.data.repository;

import com.sergo.interview.data.entity.book.Book;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookRepository extends JpaRepository<Book, Long> {
}
