package com.sergo.interview.data.service;

import com.sergo.interview.data.entity.book.Book;
import com.sergo.interview.data.repository.BookRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class BookDataService {

	private final BookRepository bookRepository;

	public BookDataService(BookRepository bookRepository) {
		this.bookRepository = bookRepository;
	}

	public Book save(Book book){
		return bookRepository.save(book);
	}

	public Optional<Book> findById(Long id){
		return bookRepository.findById(id);
	}
}
