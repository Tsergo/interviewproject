package com.sergo.interview.data.service;

import com.sergo.interview.data.entity.author.Author;
import com.sergo.interview.data.repository.AuthorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AuthorDataService extends AbstractDataService<Author, Long> {

	private AuthorRepository authorRepository;

	@Autowired
	public AuthorDataService(AuthorRepository authorRepository) {
		super(authorRepository);
		this.authorRepository = authorRepository;
	}

}
