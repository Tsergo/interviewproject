package com.sergo.interview.data.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

public abstract class AbstractDataService<T, ID extends Serializable> {

	protected JpaRepository<T, ID> repository;

	public AbstractDataService(JpaRepository<T, ID> repository) {
		this.repository = repository;
	}

	public T save(T object) {
		return repository.save(object);
	}

	public void remove(T object) {
		repository.delete(object);
	}

	public void removeById(ID id) {
		repository.deleteById(id);
	}

	public Optional<T> findById(ID id) {
		return repository.findById(id);
	}

	public List<T> findAll() {
		return repository.findAll();
	}

	public Page<T> findPageable(Pageable pageable) {
		return repository.findAll(pageable);
	}
}
