package com.sergo.interview.controller;

import com.sergo.interview.data.entity.author.Author;
import com.sergo.interview.data.service.AuthorDataService;
import com.sergo.interview.excpetion.EntityNotFoundException;
import com.sergo.interview.mapper.AuthorMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("api/author")
@RestController
public class AuthorController {

	private final AuthorMapper authorMapper;
	private final AuthorDataService authorDataService;

	public AuthorController(AuthorMapper authorMapper, AuthorDataService authorDataService) {
		this.authorMapper = authorMapper;
		this.authorDataService = authorDataService;
	}


	@GetMapping("/{id}")
	public ResponseEntity get(@PathVariable("id") Long authorId){

		Author author = authorDataService.findById(authorId).orElseThrow(EntityNotFoundException::new);

		return ResponseEntity.ok(authorMapper.authorDTOFromAuthor(author));
	}
}
