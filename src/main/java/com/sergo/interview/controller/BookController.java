package com.sergo.interview.controller;

import com.sergo.interview.data.entity.author.Author;
import com.sergo.interview.data.entity.book.Book;
import com.sergo.interview.data.service.AuthorDataService;
import com.sergo.interview.data.service.BookDataService;
import com.sergo.interview.dto.author.AuthorDTO;
import com.sergo.interview.dto.book.BookDTO;
import com.sergo.interview.excpetion.EntityNotFoundException;
import com.sergo.interview.mapper.AuthorMapper;
import com.sergo.interview.mapper.BookMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RequestMapping("api/book")
@RestController
public class BookController {

	private final BookMapper bookMapper;
	private final AuthorMapper authorMapper;
	private final BookDataService bookDataService;
	private final AuthorDataService authorDataService;


	public BookController(BookMapper bookMapper,
	                      AuthorMapper authorMapper,
	                      BookDataService bookDataService,
	                      AuthorDataService authorDataService
	) {
		this.bookMapper = bookMapper;
		this.authorMapper = authorMapper;
		this.bookDataService = bookDataService;
		this.authorDataService = authorDataService;
	}


	@GetMapping("/{id}")
	public ResponseEntity get(@PathVariable("id") Long bookId){

		Book book = bookDataService.findById(bookId).orElseThrow(EntityNotFoundException::new);

		return ResponseEntity.ok(bookMapper.bookDTOFromBook(book));

	}

	@PostMapping
	public ResponseEntity save(@RequestBody BookDTO bookDTO){

		AuthorDTO authorDTO = bookDTO.getAuthor();

		Author author = authorDataService.save(authorMapper.authorFromAuthorDTO(authorDTO));

		Book book = bookMapper.bookFromBookDTO(bookDTO);
		book.setAuthor(author);
		book = bookDataService.save(book);

		return ResponseEntity.ok(bookMapper.bookDTOFromBook(book));

	}
}
