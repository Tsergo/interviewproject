package com.sergo.interview.mapper;

import com.sergo.interview.data.entity.book.Book;
import com.sergo.interview.dto.book.BookDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-01-21T22:54:57+0400",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 1.8.0_211 (Oracle Corporation)"
)
@Component
public class BookMapperImpl implements BookMapper {

    @Autowired
    private AuthorMapper authorMapper;

    @Override
    public Book bookFromBookDTO(BookDTO bookDTO) {
        if ( bookDTO == null ) {
            return null;
        }

        Book book = new Book();

        book.setId( bookDTO.getId() );
        book.setName( bookDTO.getName() );
        book.setPage( bookDTO.getPage() );
        book.setAuthor( authorMapper.authorFromAuthorDTO( bookDTO.getAuthor() ) );

        return book;
    }

    @Override
    public BookDTO bookDTOFromBook(Book book) {
        if ( book == null ) {
            return null;
        }

        BookDTO bookDTO = new BookDTO();

        bookDTO.setId( book.getId() );
        bookDTO.setName( book.getName() );
        bookDTO.setPage( book.getPage() );
        bookDTO.setCreatedAt( book.getCreatedAt() );
        bookDTO.setUpdatedAt( book.getUpdatedAt() );

        return bookDTO;
    }

    @Override
    public List<BookDTO> bookDTOListFromBookList(List<Book> books) {
        if ( books == null ) {
            return null;
        }

        List<BookDTO> list = new ArrayList<BookDTO>( books.size() );
        for ( Book book : books ) {
            list.add( bookDTOFromBook( book ) );
        }

        return list;
    }
}
