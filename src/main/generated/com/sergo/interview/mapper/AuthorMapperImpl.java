package com.sergo.interview.mapper;

import com.sergo.interview.data.entity.author.Author;
import com.sergo.interview.data.entity.book.Book;
import com.sergo.interview.dto.author.AuthorDTO;
import com.sergo.interview.dto.book.BookDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-01-21T22:54:57+0400",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 1.8.0_211 (Oracle Corporation)"
)
@Component
public class AuthorMapperImpl implements AuthorMapper {

    @Autowired
    private BookMapper bookMapper;

    @Override
    public AuthorDTO authorDTOFromAuthor(Author author) {
        if ( author == null ) {
            return null;
        }

        AuthorDTO authorDTO = new AuthorDTO();

        authorDTO.setId( author.getId() );
        authorDTO.setName( author.getName() );
        authorDTO.setSurname( author.getSurname() );
        authorDTO.setAge( author.getAge() );
        authorDTO.setBooks( bookMapper.bookDTOListFromBookList( author.getBooks() ) );
        authorDTO.setCreatedAt( author.getCreatedAt() );
        authorDTO.setUpdatedAt( author.getUpdatedAt() );

        return authorDTO;
    }

    @Override
    public Author updateAuthor(Author author, AuthorDTO authorD) {
        if ( authorD == null ) {
            return null;
        }

        author.setName( authorD.getName() );
        author.setSurname( authorD.getSurname() );
        author.setAge( authorD.getAge() );
        if ( author.getBooks() != null ) {
            List<Book> list = bookDTOListToBookList( authorD.getBooks() );
            if ( list != null ) {
                author.getBooks().clear();
                author.getBooks().addAll( list );
            }
            else {
                author.setBooks( null );
            }
        }
        else {
            List<Book> list = bookDTOListToBookList( authorD.getBooks() );
            if ( list != null ) {
                author.setBooks( list );
            }
        }

        return author;
    }

    @Override
    public Author authorFromAuthorDTO(AuthorDTO authorDTO) {
        if ( authorDTO == null ) {
            return null;
        }

        Author author = new Author();

        author.setId( authorDTO.getId() );
        author.setCreatedAt( authorDTO.getCreatedAt() );
        author.setUpdatedAt( authorDTO.getUpdatedAt() );
        author.setName( authorDTO.getName() );
        author.setSurname( authorDTO.getSurname() );
        author.setAge( authorDTO.getAge() );
        author.setBooks( bookDTOListToBookList( authorDTO.getBooks() ) );

        return author;
    }

    protected List<Book> bookDTOListToBookList(List<BookDTO> list) {
        if ( list == null ) {
            return null;
        }

        List<Book> list1 = new ArrayList<Book>( list.size() );
        for ( BookDTO bookDTO : list ) {
            list1.add( bookMapper.bookFromBookDTO( bookDTO ) );
        }

        return list1;
    }
}
